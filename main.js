(() => {

  function createHeading() {
    const HEADING = document.createElement('h1');
    const HEADING_SMALL = document.createElement('small');
    HEADING.textContent = 'iStudentsManagement';
    HEADING.style.display = 'flex';
    HEADING.style.flexDirection = 'column';
    HEADING_SMALL.textContent = 'Панель управления студентами';
    HEADING_SMALL.style.fontSize = '20px';
    HEADING_SMALL.style.fontStyle = 'italic';
    HEADING.append(HEADING_SMALL);
    return HEADING;
  };

  function createInput(type, text) {
    const INPUT_GROUP = document.createElement('div');
    INPUT_GROUP.style.flex = '0 1 50%'
    const LABEL = document.createElement('label');
    const INPUT = document.createElement('input');
    INPUT_GROUP.classList.add('form-group');
    LABEL.textContent = text;
    INPUT.classList.add('form-control');
    INPUT.type = type;
    INPUT.required = 'true';
    let currentYear = new Date().getFullYear();

    switch(type) {
      case 'number':
        INPUT.minLength = '4';
        INPUT.maxLength = '4';
        INPUT.min = 2000;
        INPUT.max = currentYear;
        break;
      case 'text':
        INPUT.minLength = '2';
        INPUT.maxLength = '20';
      break
      case 'date':
      INPUT.min ='1900-01-01';
    }
    INPUT_GROUP.append(LABEL, INPUT);
    return {
      INPUT_GROUP,
      INPUT
    };
  };

  function createFormBtn() {
    const BTN = document.createElement('button');
    BTN.textContent = 'Добавить студента';
    BTN.style.borderRadius= '0.25rem';
    return BTN;
  };

  function createForm(STUDENTS_LIST, STUDENTS_TABLE, REQUIRED_DATA) {
    const FORM = document.createElement('form');
    const UPPER_INPUT_GROUP = document.createElement('div');
    const DOWN_INPUT_GROUP = document.createElement('div');
    const FORM_BTN = createFormBtn();
    const STUDENT_DATA = [];
    let studentRow;

    // лучше все в CSS
    FORM.classList.add('form-horizontal');
    FORM.style.padding = '20px'
    FORM.style.border = '1px solid #ced4da';
    FORM.style.borderRadius = '20px';
    UPPER_INPUT_GROUP.style.display = 'flex';
    UPPER_INPUT_GROUP.style.flexWrap = 'wrap';
    DOWN_INPUT_GROUP.style.width = '100%';
    DOWN_INPUT_GROUP.style.display = 'flex';
    DOWN_INPUT_GROUP.style.justifyContent = 'end';

    for(const DATA of REQUIRED_DATA) {
      const INPUT =  createInput(DATA.type, DATA.name);;
      UPPER_INPUT_GROUP.append(INPUT.INPUT_GROUP);
      STUDENT_DATA.push(INPUT);
    };
    DOWN_INPUT_GROUP.append(FORM_BTN);
    FORM.append(
      UPPER_INPUT_GROUP,
      DOWN_INPUT_GROUP
    );

    FORM.addEventListener('submit', (event) => {
      event.preventDefault();
      const STUDENT = {};
      for(let i = 0; i < REQUIRED_DATA.length; ++i) {
        const KEY = Object.values(REQUIRED_DATA[i]);
        STUDENT[KEY[2]] = STUDENT_DATA[i].INPUT.value;
      };
      STUDENTS_LIST.push(STUDENT);
      for(const INPUT of STUDENT_DATA) {
        INPUT.INPUT.value = '';
      };
      studentRow = addNewRow(STUDENTS_TABLE, STUDENT);
      //STUDENTS_LIST.sort()
      //console.log(STUDENTS_LIST);
    });
    return {
      FORM,
      FORM_BTN,
      STUDENT_DATA,
      studentRow
    };
  };

  function createTable(REQUIRED_DATA, STUDENTS_LIST) {
    const STUDENTS_TABLE = document.createElement('table');
    STUDENTS_TABLE.style.marginTop = '50px';
    STUDENTS_TABLE.classList.add('table', 'table-striped');
    const THEAD = document.createElement('thead');
    const TBODY = document.createElement('tbody');
    const HEAD_ROW = document.createElement('tr');
    const HEAD = [];
    for(const DATA of REQUIRED_DATA) {

      const CELL =  document.createElement('th');
      CELL.textContent = DATA.name;
      CELL.style.userSelect = 'none';
      CELL.style.cursor = 'pointer';



      HEAD_ROW.append(CELL);
      HEAD.push(CELL);
    };
    THEAD.append(HEAD_ROW);
    STUDENTS_TABLE.append(
      THEAD,
      TBODY
    );
    return {
      STUDENTS_TABLE,
      TBODY,
      HEAD
    };
  };

  function addNewRow(STUDENTS_TABLE, STUDENT) {
    const STUDENT_DATA_VALUE = Object.values(STUDENT);
    const ROW = document.createElement('tr');
    for(const DATA of STUDENT_DATA_VALUE) {
      const CELL =  document.createElement('td');
      CELL.textContent = DATA;
      ROW.append(CELL);
    }
    STUDENTS_TABLE.TBODY.append(ROW);
    return ROW;
  };

  function sortTable(STUDENTS_TABLE, NEW_STUDENTS_LIST) {

    for(let i = 0; i < NEW_STUDENTS_LIST.length; ++i) {

      const STUDENT_KEY = Object.keys(NEW_STUDENTS_LIST[i]);
      console.log(STUDENT_KEY);
      const ROW = document.createElement('tr');
      for(const DATA of STUDENT_KEY) {
        const CELL =  document.createElement('td');
        CELL.textContent = DATA;
        ROW.append(CELL);
      };
      STUDENTS_TABLE.TBODY.append(ROW);
    };

    return STUDENTS_TABLE;
  };



  const REQUIRED_DATA = [
    {name: 'Фамилия', type: 'text', engName: 'surname'},
    {name: 'Имя', type: 'text', engName: 'name'},
    {name: 'Отчество', type: 'text', engName: 'patronymic'},
    {name: 'Дата рождения', type: 'date', engName: 'datebBirth'},
    {name: 'Факультет', type: 'text', engName: 'faculty'},
    {name: 'Год начала обучения', type: 'number', engName: 'startStudyYear'}
  ];
  const STUDENTS_LIST = [];
  const HEADING = createHeading();
  const STUDENTS_TABLE = createTable(REQUIRED_DATA, STUDENTS_LIST);
  const FORM = createForm(STUDENTS_LIST, STUDENTS_TABLE, REQUIRED_DATA);
  const CONTAINER = document.getElementById('container');

  CONTAINER.append(
    HEADING,
    FORM.FORM,
    STUDENTS_LIST,
    STUDENTS_TABLE.STUDENTS_TABLE
  );


  for(const HEAD_NAME of STUDENTS_TABLE.HEAD) { // цикл для создания действия на каждый заголовок
    HEAD_NAME.addEventListener('click', (event) => {

      const NEW_ARRAY = [];

      for(const DATA of REQUIRED_DATA) {
        const DATA_KEY = Object.values(DATA);

        if(event.currentTarget.textContent === DATA_KEY[0]) {
          console.log(DATA_KEY);
          console.log(`Это ${DATA_KEY[0]}, ${DATA_KEY[2]}`);
          console.log(STUDENTS_LIST);

          for(const STUDENT of STUDENTS_LIST) {
            const KEYS = Object.keys(STUDENT);
            const VALUES = Object.values(STUDENT);
            console.log(KEYS);
            console.log(VALUES);

            for(let i = 0; i < KEYS.length; ++i) {

              if(KEYS[i] === DATA_KEY[2]) {
                console.log(`Сортировка по ${KEYS[i]}`);
                NEW_ARRAY.push(KEYS[i]);
              }

            }
            console.log(NEW_ARRAY);
            // const NEW_STUDENT = {};


          }

        };

        // for(let i = 0; i < REQUIRED_DATA.length; ++i) {
        //   const DATA_KEY = Object.values(REQUIRED_DATA[i]);

        //   if(event.currentTarget.textContent === DATA_KEY[0]) {
        //     console.log(DATA_KEY);
        //     console.log(`Это ${DATA_KEY[0]}, ${DATA_KEY[2]}`);
        //     console.log(STUDENTS_LIST);

        //     for (let i = 0; i < STUDENTS_LIST.length; i++) {
        //       const KEYS = Object.keys(STUDENTS_LIST[i]);
        //       const VALUES = Object.values(STUDENTS_LIST[i]);
        //       console.log(KEYS);
        //       console.log(VALUES);

        //     }
        //     STUDENTS_LIST.sort();

        //     // (a, b) => a - b
        //     //const NEW_STUDENTS_LIST = STUDENTS_LIST.sort();

        //     //sortTable(STUDENTS_TABLE, NEW_STUDENTS_LIST)
        //   };

      };

      //console.log(event.currentTarget.textContent);


      // CELL.addEventListener('click', (event) => {
      //   console.log(DATA_KEY[2]);
      //   for(const CELL of HEAD){

      //   };
      // });

    })
  }

  // STUDENTS_TABLE.HEAD.NAME.addEventListener('click', (event) => {
  //   event.target.style.color = 'red';
  // })





})();
